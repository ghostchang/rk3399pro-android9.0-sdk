###############################################################################
# GPS HAL libraries
LOCAL_PATH := $(call my-dir)

PRODUCT_COPY_FILES += \
			hardware/rockchip/gps/TD1030HAL/AGNSS/64/libtdcrypto.so:vendor/lib64/libtdcrypto.so \
			hardware/rockchip/gps/TD1030HAL/AGNSS/64/libtdssl.so:vendor/lib64/libtdssl.so \
			hardware/rockchip/gps/TD1030HAL/AGNSS/64/libtdsupl.so:vendor/lib64/libtdsupl.so \
			hardware/rockchip/gps/TD1030HAL/AGNSS/64/supl-client:vendor/bin/supl-client \
			hardware/rockchip/gps/TD1030HAL/tdgnss.conf:vendor/etc/tdgnss.conf \
			hardware/rockchip/gps/TD1030HAL/AGNSS/getagpsdata.sh:vendor/bin/getagpsdata.sh \
			hardware/rockchip/gps/TD1030HAL/AGNSS/gps.default.so:vendor/lib64/hw/gps.default.so
