#ifndef __UPDATE_H_
#define __UPDATE_H_

#define UPDATE_ING              (1)
#define UPDATE_SUCCESS          (0)
#define UPDATE_ERROR            (-1)
#define UPDATE_COM_TX_ERROR        (-2)
#define UPDATE_DATA_MALLOC_ERROR   (-3)
#define UPDATE_TRANS_ERROR         (-4)

int update_td_handler_return(unsigned char *data);

int update_td_enter_update(void);

int update_td_init(const unsigned char *update_data, int data_len, int comid);

void update_td_get_sync(int *sync_now, int *sync_total);
void* update_td_handler_thread(void *arg);

#endif

