#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>

#include <linux/iio/iio.h>
#include <linux/iio/machine.h>
#include <linux/iio/driver.h>
#include <linux/iio/consumer.h>

#define FIREFLY_ADC_SAMPLE_JIFFIES	(50 / (MSEC_PER_SEC / HZ))
#define FIREFLY_FAN_ABSOLUTE_VALUE  600
#define VREF 1800

struct iio_channel *chan;
struct delayed_work adc_poll_work;
bool fan_insert;
int count;

static void rp_demo_adc_poll(struct work_struct *work)
{
    int ret,raw;
    int result = -1;

    ret = iio_read_channel_raw(chan, &raw); 
    if (ret < 0) {
        printk("read hook adc channel() error: %d\n", ret);
        return;
    }
    if(raw < FIREFLY_FAN_ABSOLUTE_VALUE && !fan_insert){
        if(count < 8) count++;
	else{
        	fan_insert = true;
		count = 0;
        	result = (VREF*raw)/1023;//Vref / (2^n-1) = Vresult / raw
        	printk("Fan insert! raw= %d Voltage= %dmV\n",raw,result);
	}
    }
    else if(raw > FIREFLY_FAN_ABSOLUTE_VALUE && fan_insert){
	if(count < 8) count++;
	else{
        	fan_insert = false;
		count = 0;
        	result = (VREF*raw)/1023;
        	printk("Fan out! raw= %d Voltage=%dmV\n",raw,result);
	}
    }
    else count = 0;
    schedule_delayed_work(&adc_poll_work, FIREFLY_ADC_SAMPLE_JIFFIES);
}

static int rp_adc_probe(struct platform_device *pdev)
{
    printk("rp_adc_probe!\n");

    count = 0;
    chan = iio_channel_get(&(pdev->dev), NULL);
    if (IS_ERR(chan))
 	       	{
			chan = NULL;
			printk("%s() have not set adc chan\n", __FUNCTION__);
            return -1;
	}

    fan_insert = false;
    if (chan) {
		INIT_DELAYED_WORK(&adc_poll_work, rp_demo_adc_poll);
		schedule_delayed_work(&adc_poll_work,1000);
	}
    return 0;
    
}

static int rp_adc_remove(struct platform_device *pdev)
{
    printk("rp_adc_remove!\n");
    iio_channel_release(chan);
    return 0;
}

static const struct of_device_id rp_adc_match[] = { 
    { .compatible = "rongpin,rk3399-adc" },
    {},
};

static struct platform_driver rp_adc_driver = { 
    .probe      = rp_adc_probe,
    .remove     = rp_adc_remove,
    .driver     = { 
        .name   = "rp_adc",
        .owner  = THIS_MODULE,
        .of_match_table = rp_adc_match,
    },  
};

static int rp_adc_init(void)
{
	return platform_driver_register(&rp_adc_driver);
}
late_initcall(rp_adc_init);

static void rp_adc_exit(void)
{
	platform_driver_unregister(&rp_adc_driver);
}
module_exit(rp_adc_exit);


MODULE_AUTHOR("rpdzkj");
MODULE_DESCRIPTION("gpio j60 driver for rpdzkj");
MODULE_LICENSE("GPL");

